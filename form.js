    // *****************************User Validation Starts*****************************
    const fname = document.querySelector('#fname');
    fname.addEventListener('input', () => {
      fname.value = fname.value ? fname.value.trimStart() : ''; 
    })
    // fname=fname.replace(/\s\s+/g,'');
    function validateuser(){
      var subUserError
      var user = document.getElementById('fname').value;
      // user.replace(/^\s+|\s+$/g, "");

      var pattern=/[^A-Za-z]+\s$/;
      // var pattern=/^[A-Za-z]+\s{1}[A-Za-z]+/g
      // var pattern=/([A-Z]{1})([a-z]+)(\s)([A-Z]{1})([a-z]+){1}/g
      // var pattern=/[A-Za-z]+\s[A-Za-z]+$/
      
     

      if(user==""){
        document.getElementById('username').style.color="red";
        document.getElementById('username').innerHTML="*Please enter your name!!";
        document.getElementById('submit').disabled=true;
        return false;
      }
      else if(user.match(pattern)){ 
             document.getElementById('username').style.color="red";
             document.getElementById('username').innerHTML="not valid";
             document.getElementById('submit').disabled=true;
             return false
           }

            else if(user.length<3){
              document.getElementById('username').style.color="red";
              document.getElementById('username').innerHTML="*Invalid Name! Atleast 3 characters required";
              document.getElementById('submit').disabled=true;
            return false
          }
          else{
            document.getElementById('fname').style.borderColor="green";
            document.getElementById('username').innerHTML="";
            document.getElementById('submit').disabled=false;
            return true
          }

    //       if(user.keyCode==32){
    //         const fname = document.querySelector('#fname');
    //         fname.addEventListener('input', () => {
    //         fname.value = fname.value ? fname.value.trim() : ''; 
    // })
    //       }


        }



    $(function() {
    $("input[name='name']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
    })
    // *****************************User Validation Ends*****************************


    


    // *****************************E-mail Validation Starts*****************************
    const fEmail = document.querySelector('#femail');
    fEmail.addEventListener('input', () => {
      fEmail.value = fEmail.value ? fEmail.value.trimStart() : ''; 
    })

    function validateemail(){
      var subEmailError
      var mail = document.getElementById('femail').value;

      var pattern=/[a-zA-Z0-9]+[@][a-z]+[\.][a-z]{2,3}$/;
      if(mail==""){
        document.getElementById('usermail').style.color="red"
        document.getElementById('usermail').innerHTML="*Please enter your email!!";
        document.getElementById('submit').disabled=true;
        return false;
      }

        // if((mail.indexOf('@')<=0)||(mail.indexOf('.')<=0)){
        //   document.getElementById('usermail').style.color="red";
        //   document.getElementById('usermail').innerHTML="*Invalid email";
        // }

      else if(pattern.test(mail)==false){
        document.getElementById('usermail').style.color="red";
        document.getElementById('usermail').innerHTML="not valid";
        document.getElementById('submit').disabled=true;
        return false;
      }else{
          document.getElementById('femail').style.borderColor="green";
          document.getElementById('usermail').innerHTML="";
          document.getElementById('submit').disabled=false;
          return true
        }
      }
    // *****************************E-mail Validation Ends*****************************


    // && (mail.charAt(mail.length-3)!='.')


    // *****************************Phone Validation Starts*****************************
    const fPhone = document.querySelector('#fphone');
    fPhone.addEventListener('input', () => {
      fPhone.value = fPhone.value ? fPhone.value.trimStart() : ''; 
    })


    function validatephone(){
      var subPhoneError
      var phn = document.getElementById('fphone').value;

      var pattern=/^[6789][0-9]{9}$/;
      if(phn==""){
       document.getElementById('userphone').innerHTML="";
       document.getElementById('submit').disabled=true;
       return false;

     }
     else if(pattern.test(phn)==false){
      document.getElementById('userphone').style.color="red";
      document.getElementById('userphone').innerHTML="not valid";
      document.getElementById('submit').disabled=true;
      return false
    }else{
      document.getElementById('fphone').style.borderColor="green";
      document.getElementById('userphone').innerHTML="";
      document.getElementById('submit').disabled=false;
      return true
      }
    }    
    // *****************************Phone Validation Ends*****************************





    // *****************************Password Validation Starts*****************************
    const fPassword = document.querySelector('#fpass');
    fPassword.addEventListener('input', () => {
      fPassword.value = fPassword.value ? fPassword.value.trimStart() : ''; 
    })


    function validatepassword(){
      var subPasswordError
      var pass = document.getElementById('fpass').value;
      var conpass = document.getElementById('f-cpass').value;

        var pattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%#*?^&])[A-Za-z\d@$#^!%*?&]{8,}$/;
      // var pattern=/^[a-zA-Z0-9]+[@]+[a-zA-Z0-9]+{7,}$/;  
      if(pattern.test(pass) == false){
        document.getElementById('userpassword').style.color="red";
        document.getElementById('userpassword').innerHTML="Not-Valid";
        document.getElementById('submit').disabled=true;
        return false
      }
      else{
          document.getElementById('fpass').style.borderColor="green";
          document.getElementById('userpassword').innerHTML="";
          document.getElementById('submit').disabled=false;
          return true
        }
      }

    // min. 8 characters, 1 uppercase, 1 lowercase, 1 no. & 1 special character
    // ***************************** Password Validation Ends *****************************



    // ********************************* TOGGLE EYE *****************************************


    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        input = $(this).parent().find("input");
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
      });
        

    // const togglePassword=document.querySelector('#togglePassword');
    // const password = document.querySelector('#fpass');
    // togglePassword.addEventListener('click', function(e) {
    //   // toggle the type attribute
    //   const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    //   password.setAttribute('type', type);
    //   // toggle the eye slash icon
    //   this.classList.toggle('fa-eye');
    // });


    // *****************************Confirm Password Validation Starts*****************************
    const fConPassword = document.querySelector('#f-cpass');
    fConPassword.addEventListener('input', () => {
      fConPassword.value = fConPassword.value ? fConPassword.value.trimStart() : ''; 
    })


    function validateconfirmpassword(){
      var subPasswordError
      var subCPasswordError
      var pass = document.getElementById('fpass').value;
      var conpass = document.getElementById('f-cpass').value;
       // var pattern=/^[a-zA-Z0-9]+[@]+[a-zA-Z0-9]+/
       var pattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;


       if(conpass==""){
        document.getElementById('user-confirm-password').style.color="red"
        document.getElementById('user-confirm-password').innerHTML="*Please confirm your password!!";
        document.getElementById('submit').disabled=true;
        return false
      }

      else if(pass != conpass){
        document.getElementById('user-confirm-password').style.color="red";
        document.getElementById('user-confirm-password').innerHTML="*Paasword does not match!";
        document.getElementById('submit').disabled=true;
        return false
      }
      else if((pass==conpass)||(pass.length==conpass.length)){
        document.getElementById('fpass').style.borderColor="green";
        document.getElementById('userpassword').innerHTML="";
        document.getElementById('user-confirm-password').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true
      }
      else{
        document.getElementById('f-cpass').style.borderColor="green";
        document.getElementById('user-confirm-password').innerHTML="";
        document.getElementById('userpassword').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true
        }
      }
    // *****************************Confirm Password Validation Ends*****************************



       // **************************** OTP JS CODE **********************

document.addEventListener("DOMContentLoaded", function(event) {

function OTPInput() {
const inputs = document.querySelectorAll('#otp > *[id]');
for (let i = 0; i < inputs.length; i++) { 
   inputs[i].addEventListener('keydown', function(event) { if (event.key==="Backspace" ) 
      { inputs[i].value='' ; if (i !==0) inputs[i - 1].focus(); } else { 
         if (i===inputs.length - 1 && inputs[i].value !=='' ) { return true; } 
         else if (event.keyCode> 47 && event.keyCode < 58) { inputs[i].value=event.key; 
       if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } 
      else if (event.keyCode> 64 && event.keyCode < 91) { inputs[i].value=String.fromCharCode(event.keyCode); 
      if (i !==inputs.length - 1) inputs[i + 1].focus(); event.preventDefault(); } } }); } } OTPInput(); });


    // var check=()=>{
    //   if(pass.length!=conpass.length){
    //     document.getElementById('userpassword').innerHTML"";
    //     document.getElementById('user-confirm-password').innerHTML="Password does not match/."
    //   }
    // }
   


    // *****************************Address Validation Starts*****************************
    const fAdd = document.querySelector('#faddress');
    fAdd.addEventListener('input', () => {
      fAdd.value = fAdd.value ? fAdd.value.trimStart() : ''; 
    })


    function validateaddress(){
      var subAddressError
      var addr = document.getElementById('faddress').value;
      var pattern=/^[A-Za-z]+$/
      if(addr==""){
        document.getElementById('useraddress').style.color="red"
        document.getElementById('useraddress').innerHTML="*Address is required";
        document.getElementById('submit').disabled=true;
        return false;
      }else if(addr.match(pattern)){
        // document.getElementById('useraddress').style.color="red";
        document.getElementById('useraddress').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true;
      }
      else{
        document.getElementById('faddress').style.borderColor="green";
        document.getElementById('useraddress').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true;
      }

    }

    // [^@#$%^&*"'?><+=~`!|]+    /*****/"NOT ALLOWED"/*****/



     $(function() {
    $("input[name='address']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
    })
    // *****************************Address Validation Ends*****************************





    // *****************************CITY VALIDATION STARTS *****************************
    const fCity = document.querySelector('#fcity');
    fCity.addEventListener('input', () => {
      fCity.value = fCity.value ? fCity.value.trimStart() : ''; 
    })


    function validatecity(){
      var subCityError
      var city = document.getElementById('fcity').value;
          // var pattern = /[^0-9]/
          // console.log("city",city)
          var pattern = /^[A-Za-z]\s+$/
          if(city==""){
            document.getElementById('usercity').style.color="red";
            document.getElementById('usercity').innerHTML="*City name required!!";
            document.getElementById('submit').disabled=true;
            return false;
          }
          else if(city.match(pattern)){
            document.getElementById('usercity').style.color="red";
            document.getElementById('usercity').innerHTML="not valid city name";
            document.getElementById('submit').disabled=true;
            return false

          }else{
            document.getElementById('fcity').style.borderColor="green";
            document.getElementById('usercity').innerHTML="";
            document.getElementById('submit').disabled=false;
            return true
          }
        }


         $(function() {
    $("input[name='city']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
    })

        // pattern.test(city)==false
    // *****************************CITY VALIDATION ENDS*****************************





    // *****************************PIN VALIDATION STARTS*****************************
    const fPin = document.querySelector('#fpin');
    fPin.addEventListener('input', () => {
      fPin.value = fPin.value ? fPin.value.trimStart() : ''; 
    })


    function validatepin(){
      var subPinError
      var pin = document.getElementById('fpin').value;
      var pattern=/^[1-9]{1}[0-9]{2}\s{0,1}[0-9]{3}$/;
      if(pin==""){
        document.getElementById('user-city-pin').style.color="red";
        document.getElementById('user-city-pin').innerHTML="*City pin required!!";
        document.getElementById('submit').disabled=true;
        return false
      }
      else if(pattern.test(pin)==false){
        document.getElementById('user-city-pin').style.color="red";
        document.getElementById('user-city-pin').innerHTML="*City pin Invalid!!";
        document.getElementById('submit').disabled=true;
        return false
      }
      else{
        document.getElementById('fpin').style.borderColor="green";
        document.getElementById('user-city-pin').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true

      }
    }



//     function fetchapi(){
//    var pincode = document.getElementById("fpin").value;
//     fetch(`https://api.postalpincode.in/pincode/${pincode}`).then((response) => response.json())
//       .then((data) => {
//         var city = data[0].PostOffice[0].District;
//         var state = data[0].PostOffice[0].State;
//         var country = data[0].PostOffice[0].Country;
//         document.getElementById("fcountry").value = country;
//         document.getElementById("fcity").value=city;
//         document.getElementById("fstate").value=state;
//     });
//       if (pincode == "") {
//             document.getElementById("fcountry").value 
//         document.getElementById("fcity").value="";
//         document.getElementById("fstate").value="";

//       }
// }
    // *****************************PIN VALIDATION ENDS*****************************





    // *****************************STATE VALIDATION STARTS*****************************
    const fState = document.querySelector('#fstate');
    fState.addEventListener('input', () => {
      fState.value = fState.value ? fState.value.trimStart() : ''; 
    })


    function validatestate(){
      var subStateError
      var state = document.getElementById('fstate').value;

      var pattern = /[A-Za-z]+[^0-9]/ 
      if(state==""){
        document.getElementById('user-state').style.color="red";
        document.getElementById('user-state').innerHTML="*State is required!!";
        document.getElementById('submit').disabled=true;
        return false;
      }
      if(state.match(pattern)){
        document.getElementById('fstate').style.borderColor="green";
        document.getElementById('user-state').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true;
      }else{
        document.getElementById('user-state').style.color="red";
        document.getElementById('user-state').innerHTML="Not Valid";
        document.getElementById('submit').disabled=true;
        return false;
      } 
    }


         $(function() {
    $("input[name='state']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
    })
    // *****************************STATE VALIDATION ENDS*****************************





    // *****************************COUNTRY VALIDATION STARTS*****************************
    const fCountry = document.querySelector('#fcountry');
    fCountry.addEventListener('input', () => {
      fCountry.value = fCountry.value ? fCountry.value.trimStart() : ''; 
    })



    function validatecountry(){
      // var subCountryError
      var country = document.getElementById('fcountry').value;
      var pattern = /[A-Za-z]+[^0-9]/ 
      if(country==""){
        document.getElementById('user-country').style.color="red";
        document.getElementById('user-country').innerHTML="*Please enter your country!!";
        document.getElementById('submit').disabled=true;
        return false
      }
      if(country.match(pattern)){
        document.getElementById('fcountry').style.borderColor="green";
        document.getElementById('user-country').innerHTML="";
        document.getElementById('submit').disabled=false;
        return true
      }else{
        document.getElementById('user-country').style.color="red";
        document.getElementById('user-country').innerHTML="Not Valid";
        document.getElementById('submit').disabled=true;
        return false
      }
    }


        $(function() {
    $("input[name='country']").on('input', function(e) {
    console.log('==>' , $(this).val().charAt(3))
    if ($(this).val().indexOf("  ") != -1) {
      $(this).val($(this).val().replace("  ", " "));
      // $(this).val($(this).val().replace(".", ""));
    } else {
      $(this).val($(this).val().replace(".", ""));
    }
     });
    })
    // *****************************COUNTRY VALIDATION ENDS*****************************






    // *****************************OTP VALIDATION ENDS*****************************
  function clickEvent(first,last){
    if(first.value.length){
      document.getElementById(last).focus();
    }
  }
    // *****************************OTP VALIDATION ENDS*****************************




    // *****************************XML HTTP Request VALIDATION Starts*****************************
    function fetchapi(){
   var pincode = document.getElementById("fpin").value;
    fetch(`https://api.postalpincode.in/pincode/${pincode}`).then((response) => response.json())
      .then((data) => {
        var city = data[0].PostOffice[0].District;
        var state = data[0].PostOffice[0].State;
        var country = data[0].PostOffice[0].Country;
        document.getElementById("fcountry").value = country;
        document.getElementById("fcity").value=city;
        document.getElementById("fstate").value=state;
    });
      if (pincode == "") {
            document.getElementById("fcountry").value = "";
        document.getElementById("fcity").value="";
        document.getElementById("fstate").value="";

      }
}
    // *****************************XML HTTP Request VALIDATION ENDS*****************************

    function submitForm(){
      var subUserError= validateuser();
      var subEmailError= validateemail();
      var subPhoneError= validatephone();
      var subPasswordError= validatepassword();
      var subCPasswordError= validateconfirmpassword();
      var subAddressError= validateaddress();
      var subCityError= validatecity();
      var subPinError= validatepin();
      var subStateError= validatestate();
      var subCountryError= validatecountry();


      if(subUserError==true && subEmailError==true && subPhoneError==true && 
        subPasswordError==true && subCPasswordError ==true && subAddressError==true && 
        subCityError==true && subPinError==true && subStateError==true && subCountryError==true)
      {
        document.getElementById('submit').disabled=false;
        document.getElementById("myForm").submit();
        console.log('submitted')

      }else{
        // document.getElementById('submit').disabled=false;
        console.log('error')
        alert('error')
        document.getElementById('submit').disabled=true;
        
      }
}


 function ppass(){
    var password = document.getElementById("fpass").value;
    var confirm_password = document.getElementById("f-cpass").value; 
    if(password == confirm_password || confirm_password == "" ){
      
      document.getElementById('user-confirm-password').innerHTML = "";

      document.getElementById('user-confirm-password').style.color = "";
    }
    else{
      document.getElementById('user-confirm-password').innerHTML = "not match";

      document.getElementById('user-confirm-password').style.color = "red";
    }
  }
