<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="style.css">
    <title> Signup</title>
  </head>
  <body>
    <section>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-lg-7">
          <div class="picsize">
            <img src="pic.png" class="mt-5" alt="Italian Trulli" width="500" height="600">
          </div>
        </div>
        <div class="col-md-6 col-lg-5">
          <div class="card shadow" style="border-radius: 15px;">
            <div class="card-body ">
              <h2 class="text-uppercase text-center text-white">SIGN UP</h2>
              <form action="sub.php" method="POST">
                           
                    <!-- ********************** NAME ********************** -->

                    <div class="details mt-2">
                      <div class="form-group mar">
                       
                        <input type="text" id="fname" name="name" class="form-control form-control-lg border relative" onkeyup=" validateuser()" placeholder="Full Name" onpaste="return false" autocomplete="off" required onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)"/>
                          <span id="username" name=uname class="form-error font-weight-bold absolute"></span>
                      </div>

                       <!--  ********************** EMAIL ********************** -->
             
                      <div class="form-group mar">
                        <input type="email" id="femail" name="email" class="form-control form-control-lg border relative" onkeyup="return validateemail()" placeholder="Email" onpaste="return false" autocomplete="off" required/>
                        <span id="usermail" class="form-error font-weight-bold absolute"></span>
                      </div>

                      <!-- ********************** PHONE ********************** -->

                      <div class="form-group mar">
                        <input type="phone" id="fphone" name="phone" class="form-control form-control-lg border number-dropdown relative" onkeyup="return validatephone()" maxlength="10" onpaste="return false" autocomplete="off" placeholder="Phone" required  onkeypress = "return  (event.charCode > 46 && event.charCode < 58)"/>
                        <span id="userphone" class="form-error font-weight-bold absolute"></span>
                      </div>

                      <!-- ********************** PASSWORD ********************** -->

                      <div class="form-group mar">
                        <input type="password" id="fpass" name="pass" name="password" class="form-control form-control-lg border relative" onkeyup=" validatepassword();ppass()" onpaste="return false" autocomplete="off" placeholder="Password" required/>
                        <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                        <span id="userpassword" class="form-error font-weight-bold absolute"></span>
                      </div>

                      <!-- ********************** CONFIRM PASSWORD ********************** -->


                      <div class="form-group mar">
                        <input type="password" id="f-cpass" name="cpass" class="form-control form-control-lg border relative" onkeyup=" validateconfirmpassword();phas()" onpaste="return false" autocomplete="off" placeholder="Confirm Password" required/>
                        <span id="user-confirm-password" class="form-error font-weight-bold absolute"></span>
                      </div>

                       <!-- ********************** GENDER ********************** -->



                    <div class="form-group ">
                      <div class="row d-flex justify-content-around align-items-center" required />
                        <label for="text" class="col-form-label text-white d-block">Gender</label>
                       
                         <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline1" value="male" name="gender" class="custom-control-input">
                      <label class="custom-control-label required" for="customRadioInline1">Male</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" value="female" name="gender" class="custom-control-input">
                      <label class="custom-control-label required" for="customRadioInline2">Female</label>
                       </div>
                      </div>
                   </div>

                       <!-- ********************** ADDRESS ********************** -->


                      <div class="form-group mar">
                        <input type="text" id="faddress" name="address" class="form-control form-control-lg border relative" onkeyup="return validateaddress()" onpaste="return false" autocomplete="off" placeholder="Address" required onkeypress="return (event.charCode > 96 && event.charCode < 123) || // numbers
                        (event.charCode >= 65 && event.charCode <= 90) || // letters
                        event.charCode == 32" />
                        <span id="useraddress" class="form-error font-weight-bold absolute"></span>
                      </div>


                          <!-- ********************** PIN ********************** -->

                       <div class="row">
                      <div class="col-6">
                          <div class="form-group mar">
                            <input type="text" id="fpin" name="pin" class="form-control form-control-lg border number-dropdown relative" onfocusout="fetchapi()" onkeyup="return validatepin()" placeholder="Pin" onpaste="return false" autocomplete="off" required maxlength="6" onkeypress = "return  (event.charCode > 46 && event.charCode < 58)"/>
                            <span id="user-city-pin" class="form-error font-weight-bold absolute"></span>
                          </div>
                        </div>

                        <!-- ********************** CITY ********************** -->


                       
                         <div class="col-6">
                            <div class="form-group mar">
                            <input type="text" id="fcity"  name="city" class="form-control form-control-lg border relative" onkeyup="return validatecity()" placeholder="City" onpaste="return false" autocomplete="off" required onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)"/>
                            <span id="usercity" class="form-error font-weight-bold absolute"></span>
                            </div>
                         </div>
                      
                       <!-- ********************** STATE ********************** -->


                       <div class="col-6">
                          <div class="form-group mar">
                            <input type="text" id="fstate" name="state" class="form-control form-control-lg border relative" onkeyup="return validatestate()" placeholder="State" onpaste="return false" autocomplete="off" required onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)"//>
                            <span id="user-state" class="form-error font-weight-bold absolute"></span>
                          </div>
                       </div>

                        <!-- ********************** COUNTRY ********************** -->


                        <div class="col-6">
                          <div class="form-group mar">
                            <input type="text" id="fcountry" name="country" class="form-control form-control-lg border relative" onkeyup="return validatecountry()" placeholder="Country" onpaste="return false" autocomplete="off" required onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)"//>
                            <span id="user-country" class="form-error font-weight-bold absolute"></span>
                          </div>
                        </div>
                      </div>

                      <div class="d-flex justify-content-center ">
                        <button type="submit" name="submit" value="Submit" id="submit" onclick="submitForm()" class="btn btn-block btn-lg text-white btn_signup"><span class="text_signup">Sign up</span></button>
                      </div>

                      
                        <p class="text-center text-white"> Have an account ? &nbsp; <a href="login.3.html" class="fw-bold text-azure">Login </a>
                        </p>
                      </div>
                    </div>
                  </form>
                  <!-- signup ENDS here -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"></script>
  <script src="form.js"></script>
    
</script>
</body>
  </html>
