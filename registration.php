<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" >
      <link rel="stylesheet" type="text/css" href="style.css">
      <title></title>


   </head>
   <body>
      <div class="container">
  <div class="row">
    <div class="col-md-6 col-lg-7">

     <div class="picsize">
         <img src="pic.png" class="mt-5" alt="Italian Trulli" width="500" height="600">
      </div>
    </div>
    <div class="col-md-6 col-lg-5">
      <div class="card shadow" style="border-radius: 15px;">
            <div class="card-body ">
               <h2 class="text-uppercase text-center text-white">REGISTER NOW</h2>
               <form action="reg.php" method="POST">

                <div class="form-group mar">
                       
                        <input type="text" id="fname" name="name" class="form-control form-control-lg border relative" onkeyup=" validateuser()" placeholder="Full Name" onpaste="return false" autocomplete="off" required onkeypress = "return  (event.charCode > 96 && event.charCode < 123) || (event.charCode > 64 && event.charCode < 91) || (event.charCode == 32)"/>
                          <span id="username" name=uname class="form-error font-weight-bold absolute"></span>
                      </div>



                   <div class="form-group mar">
                        <input type="email" id="femail" name="email" class="form-control form-control-lg border relative" onkeyup="return validateemail()" placeholder="Email" onpaste="return false" autocomplete="off" required/>
                        <span id="usermail" class="form-error font-weight-bold absolute"></span>
                      </div>
                  
                 <div class="form-group mar">
                        <input type="phone" id="fphone" name="phone" class="form-control form-control-lg border number-dropdown relative" onkeyup="return validatephone()" maxlength="10" onpaste="return false" autocomplete="off" placeholder="Phone" required  onkeypress = "return  (event.charCode > 46 && event.charCode < 58)"/>
                        <span id="userphone" class="form-error font-weight-bold absolute"></span>
                      </div>

                     <!-- ********************** PASSWORD ********************** -->

                      <div class="form-group mar">
                        <input type="password" id="fpass" name="pass" class="form-control form-control-lg border relative" onkeyup=" validatepassword();ppass()" onpaste="return false" autocomplete="off" placeholder="Password" required/>
                        <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                        <span id="userpassword" class="form-error font-weight-bold absolute"></span>
                      </div>

                      <!-- ********************** CONFIRM PASSWORD ********************** -->


                      <div class="form-group mar">
                        <input type="password" id="f-cpass" name="cpass" class="form-control form-control-lg border relative" onkeyup=" validateconfirmpassword();phas()" onpaste="return false" autocomplete="off" placeholder="Confirm Password" required/>
                        <span id="user-confirm-password" class="form-error font-weight-bold absolute"></span>
                      </div>



                  <div class="d-flex justify-content-center ">
                        <button type="submit" name="submit" value="Submit" id="submit" onclick="submitForm()" class="btn btn-block btn-lg text-white btn_signup"><span class="text_signup">Sign up</span></button>
                      </div>


                 <div class="col text-center">
                    <div id="otp" class="inputs d-flex flex-row justify-content-center mt-2"> 
                       <input class="m-2 text-center form-control rounded" type="text" id="first" maxlength="1" /> 
                       <input class="m-2 text-center form-control rounded" type="text" id="second" maxlength="1" /> 
                       <input class="m-2 text-center form-control rounded" type="text" id="third" maxlength="1" /> 
                       <input class="m-2 text-center form-control rounded" type="text" id="fourth" maxlength="1" /> 
                       <input class="m-2 text-center form-control rounded" type="text" id="fifth" maxlength="1" /> 
                       <input class="m-2 text-center form-control rounded" type="text" id="sixth" maxlength="1" />
                    </div>
                   </div>
                  

                  <h6 class="text-uppercase text-center text-white mt-2">OR</h6>

                 <div class="d-flex justify-content-center">
                     <button type="button"
                        class="btn btn-block btn-lg gradient-custom-4" style="background-color: white;"> <i class="fa fa-google"></i> Log in with google</button>
                  </div>
                  
                  <p class="text-center text-white">Have an account ? &nbsp;<a href="login.4.html"
                     class="fw-bold text-azure">Login </a></p>
               </form>
            </div>
         </div>
      </div>
  </div>


  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
 <script src="form.js"></script>

   </body>
</html>
