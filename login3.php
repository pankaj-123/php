<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" >
      <link rel="stylesheet" type="text/css" href="style.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
      <title>Login with Google</title>


   </head>
   <body>
      <div class="container">
  <div class="row">
    <div class="col-md-6 col-lg-7">

     <div class="picsize">
         <img src="pic.png" class="mt-5" alt="Italian Trulli" width="500" height="600">
      </div>
    </div>
    <div class="col-md-6 col-lg-5">
      <div class="cardn shadow" style="border-radius: 15px;">
            <div class="card-body ">
               <h2 class="text-uppercase text-center text-white">LOGIN</h2>
               <form action="log.php" method="POST">

               
  
                    <div class="form-group mar">
                        <input type="email" id="femail" name="email" class="form-control form-control-lg border relative" onkeyup="return validateemail()" placeholder="Email" onpaste="return false" autocomplete="off" required/>
                        <span id="usermail" class="form-error font-weight-bold absolute"></span>
                      </div>
                      
                     <div class="form-group mar">
                        <input type="password" id="fpass" name="pass" class="form-control form-control-lg border relative" onkeyup="return validatepassword()" onpaste="return false" autocomplete="off" placeholder="Password" required/>
                        <i class="toggle-password fa fa-fw fa-eye-slash kgf"></i>
                        <span id="userpassword" class="form-error font-weight-bold absolute"></span>
                      </div>


                      <div class="col">
                      <p class="text-right">  <a href="#" class="text-right">Forgot Password</a></p>
                     </div>
            
                  <!-- <div class="d-flex justify-content-center">
                     <button type="button"
                        class="btn btn-block btn-lg gradient-custom-4 text-white"
                        >Login</button>
                  </div> -->

                  <div class="d-flex justify-content-center ">
                        <button type="submit" value="Submit" name="submit" id="submit" onclick="submitForm()" class="btn btn-block btn-lg text-white btn_signup"><span class="text_signup">Login</span></button>
                      </div>


                 
                  <h6 class="text-uppercase text-center text-white mt-2">OR</h6>

                 <div class="d-flex justify-content-center">
                     <button type="button"
                        class="btn btn-block bg-white btn-lg gradient-custom-4" > <i class="fa fa-google"></i>  Log in with google</button>
                  </div>
                  <p class="text-center text-white mt-2">Have an account ? &nbsp;<a href="submit.4.html"
                     class="fw-bold text-azure">Signup </a></p>
               </form>
            </div>
         </div>
      </div>
  </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script type="text/javascript">
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        console.log("sdas");
        input = $(this).parent().find("input");
             console.log("sdas2");
        if (input.attr("type") == "password") {
          input.attr("type", "text");
               console.log("sdas3");
        } else {
          input.attr("type", "password");
               console.log("sdas4");
        }
      });
        
</script>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
  <script src="form.js"></script>



   </body>
</html>
